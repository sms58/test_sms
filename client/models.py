from django.db import models


class Client(models.Model):
    phone_number = models.IntegerField()
    mobile_operator_code = models.CharField(max_length=50)
    tag = models.CharField(max_length=50)
    timezone = models.CharField(max_length=50)
