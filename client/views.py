from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from client.serializers import ClientSerializer
from client.models import Client
from django.http import Http404
from drf_yasg.utils import swagger_auto_schema


class ClientView(APIView):

    def get_client(self, pk):
        try:
            return Client.objects.get(id=pk)
        except:
            return Http404

    @swagger_auto_schema(responses={201: ClientSerializer(), 400: 'Bad Request'})
    def post(self, request):
        data = request.data
        serializer = ClientSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class ClientDetailView(APIView):
    def get_client(self, pk):
        try:
            return Client.objects.get(id=pk)
        except:
            return Http404

    @swagger_auto_schema(
        responses={202: ClientSerializer(), 400: 'Bad Request'},
        operation_description='Partial updates are allowed')
    def patch(self, request, pk):
        data = request.data
        instance = self.get_client(pk)
        serializer = ClientSerializer(instance, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(responses={204: 'No Content (Deleted)', 404: 'Not Found'})
    def delete(self, request, pk):
        instance = self.get_client(pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
