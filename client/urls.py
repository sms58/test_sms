from django.urls import path
from client.views import ClientView, ClientDetailView

urlpatterns = [
    path('', ClientView.as_view(), name='client-view'),
    path('<str:pk>/', ClientDetailView.as_view(), name='client-detail-view'),
]
