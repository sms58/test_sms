from apscheduler.schedulers.background import BackgroundScheduler
from django.utils import timezone
from message.models import Message
from .jobs import send_sms


def task(instance, scheduler):
    if timezone.now() >= instance.end_at:
        scheduler.remove_job(instance.filter)
        Message.objects.filter(status='sending').delete()
        print("Deleted all pending messages")
        return

    send_sms(instance)


def start_scheduler(instance):
    print("scheduler is called")
    scheduler = BackgroundScheduler()
    scheduler.add_job(task, 'interval', args=[
                      instance, scheduler], seconds=5, id=instance.filter)
    scheduler.start()
