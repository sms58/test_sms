import requests
import environ
from message.models import Message
from client.models import Client
from django.db.models import Q

env = environ.Env()
environ.Env.read_env()

token = 'Bearer ' + env('token')


def get_clients(tag):
    try:
        return Client.objects.filter(Q(tag=tag) | Q(mobile_operator_code=tag))
    except:
        return []


def send_sms(instance):
    print("send_sms function is called")
    clients = get_clients(instance.filter)

    for client in clients:
        message = Message.objects.create(
            status='sending', distribution=instance, client=client)

        response = requests.post('https://probe.fbrq.cloud/v1/send/0', json={
            'id': message.id,
            'phone': client.phone_number,
            'text': instance.text
        },
            headers={
                'Authorization': token
        })

        if response.status_code == 200:
            message.status = 'sent'
            message.save()
        elif response.status_code == 400:
            message.status = 'failed'
            message.save()
