from django.urls import path
from message.views import DistributionView, DistributionDetailView

urlpatterns = [
    path('', DistributionView.as_view(), name='distribution-view'),
    path('<str:pk>/', DistributionDetailView.as_view(),
         name='distribution-detail-view'),
]
