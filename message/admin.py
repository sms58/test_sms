from django.contrib import admin
from message.models import Distribution, Message

admin.site.register(Distribution)
admin.site.register(Message)
