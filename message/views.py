from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from django.db.models import F, Count, Q
from message.serializers import DistributionSerializer, StatisticSerializer, DistributionDetailSerialzier
from message.models import Distribution
from drf_yasg.utils import swagger_auto_schema


class DistributionView(APIView):

    @swagger_auto_schema(responses={200: StatisticSerializer()})
    def get(self, request):
        data = Distribution.objects.all() \
            .prefetch_related('messages') \
            .annotate(
            distribution_id=F('id'),
            total_num_messages=Count('messages'),
            num_sent=Count('messages__status', filter=Q(messages__status='sent')))

        serializer = StatisticSerializer(data, many=True)

        return Response(serializer.data)

    @swagger_auto_schema(responses={201: DistributionSerializer(), 400: 'Bad Request'})
    def post(self, request):
        data = request.data
        serializer = DistributionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class DistributionDetailView(APIView):
    def get_instance(self, pk):
        try:
            return Distribution.objects.get(id=pk)
        except:
            return Http404

    @swagger_auto_schema(responses={200: DistributionDetailSerialzier(), 404: 'Not Found'})
    def get(self, request, pk):
        data = Distribution.objects \
            .prefetch_related('messages') \
            .annotate(total_num_messages=Count('messages'),
                      num_sent=Count('messages__status', filter=Q(
                          messages__status='sent')),
                      num_failed=Count('messages__status', filter=Q(
                          messages__status='failed')),
                      num_pending=Count('messages__status', filter=Q(messages__status='sending'))) \
            .get(id=pk) \

        serializer = DistributionDetailSerialzier(data)

        return Response(serializer.data)

    @swagger_auto_schema(
        responses={202: DistributionSerializer(), 400: 'Bad Request'},
        operation_description='Partial updates are allowed')
    def patch(self, request, pk):
        data = request.data
        instance = self.get_instance(pk)
        serializer = DistributionSerializer(instance, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(responses={204: 'No Content (Deleted)', 404: 'Not Found'})
    def delete(self, request, pk):
        instance = self.get_instance(pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
