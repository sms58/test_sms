from rest_framework import serializers, fields
from message.models import Distribution, Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            'id',
            'created_at',
            'status'
        ]


class DistributionSerializer(serializers.ModelSerializer):
    start_at = serializers.DateTimeField(
        format="%Y-%m-%dT%H:%M:%S", required=False)
    end_at = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = Distribution
        fields = [
            'id',
            'text',
            'filter',
            'start_at',
            'end_at',
        ]


class StatisticSerializer(serializers.Serializer):
    distribution_id = serializers.IntegerField()
    total_num_messages = serializers.IntegerField()
    num_sent = serializers.IntegerField()
    start_at = serializers.DateTimeField()
    end_at = serializers.DateTimeField()


class DistributionDetailSerialzier(serializers.ModelSerializer):
    total_num_messages = serializers.IntegerField()
    num_sent = serializers.IntegerField()
    num_failed = serializers.IntegerField()
    num_pending = serializers.IntegerField()

    class Meta:
        model = Distribution
        fields = [
            'id',
            'text',
            'filter',
            'start_at',
            'end_at',
            'total_num_messages',
            'num_sent',
            'num_failed',
            'num_pending',
        ]
