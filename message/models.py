from django.db import models
from client.models import Client
from django.utils import timezone


class Distribution(models.Model):
    text = models.TextField()
    filter = models.CharField(max_length=50)
    start_at = models.DateTimeField(default=timezone.now())
    end_at = models.DateTimeField()

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(
                start_at__lt=models.F('end_at')), name='correct_time')
        ]


class Message(models.Model):
    created_at = models.DateField(auto_now_add=True)
    status = models.CharField(max_length=50)
    distribution = models.ForeignKey(
        Distribution, related_name='messages', on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
