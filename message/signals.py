from django.db.models.signals import post_save
from django.dispatch import receiver
from message.models import Distribution
from schedule.scheduler import start_scheduler


@receiver(post_save, sender=Distribution)
def activate_scheduler(sender, instance, created, **kwargs):
    print("Created: ", created)
    if created:
        print("Signals Working!!!")
        start_scheduler(instance)
