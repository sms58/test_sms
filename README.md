# Dependencies:
1. Django==4.0
2. django-environ==0.9.0
3. djangorestframework==3.14.0
4. drf-yasg

# Commands:
1. `cd {project_folder}`
2. `pip install -r requirements.txt`
3. `python manage.py migrate`
4. `python manage.py runserver`

# Note
* To see all APIs visit url: BaseURL/swagger
* All the requests with the given OpenAPI were failed (python requests package was used, to see the details please visit file /schedule/jobs.py)

# !Important
* I've hidden my token in env, so the program MIGHT require .env file with token in it
